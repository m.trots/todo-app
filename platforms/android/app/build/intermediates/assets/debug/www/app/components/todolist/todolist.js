const todoList = {
  templateUrl: "app/components/todolist/todolist.html",
  controller: class {
    constructor($state, TodoService)
    {
      "ngInject";
      this.$state = $state;
      this.todoService = TodoService;
      this.d = new Date();
      this.todoService.list = [];
    }

    $onInit() {
      this.todoService.getAll();

      this.date =
        this.d.getMonth() + 1 +
      "/" + this.d.getDate() +
      "/" + this.d.getFullYear();
    }

    goToEdit(id) {
      this.$state.go("detail", { id });
    };

  }
};

angular.module("app.todolist.component", []).component("todoList", todoList);
